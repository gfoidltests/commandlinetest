﻿using System.Reflection;

[assembly: AssemblyCompany("Foidl Günther")]
[assembly: AssemblyCopyright("Copyright © Foidl Günther 2012-2017")]

[assembly: AssemblyVersion("0.1.8.0")]              // git log --oneline | wc -l
[assembly: AssemblyInformationalVersion("0.1.0")]   // SemVer