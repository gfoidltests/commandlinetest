﻿using System.Reflection;
using Microsoft.Extensions.CommandLineUtils;

namespace CommandLineTest
{
    public static class RootCommandConfiguration
    {
        public static void Configure(CommandLineApplication app, CommandLineOptions options)
        {
            app.HelpOption("-?|-h|--help");
            app.VersionOption("--version", () => GetVersion(false), () => GetVersion(true));

            app.Name               = "ninja";
            app.FullName           = "ninja -- Demo for commandline parsing in .net core";
            app.Description        = "demo application for commandline parsing in .net core";
            app.ShortVersionGetter = () => GetVersion(false);
            app.LongVersionGetter  = () => GetVersion(true);

            app.Command("hide", c => HideCommandConfiguration.Configure(c, options));
            app.Command("attack", c => AttackCommandConfiguration.Configure(c, options));

            app.OnExecute(() =>
            {
                options.Command = new RootCommand(app);
                return 0;
            });
        }
        //---------------------------------------------------------------------
        private static string GetVersion(bool longForm = false)
        {
            var assembly = Assembly.GetEntryAssembly();

            if (longForm)
            {
                var version = assembly.GetCustomAttribute<AssemblyVersionAttribute>();
                return version.Version;
            }
            else
            {
                var version = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
                return version.InformationalVersion;
            }
        }
    }
}