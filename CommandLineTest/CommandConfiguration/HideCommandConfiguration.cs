﻿using Commands;
using Microsoft.Extensions.CommandLineUtils;

namespace CommandLineTest
{
    public static class HideCommandConfiguration
    {
        public static void Configure(CommandLineApplication cmd, CommandLineOptions options)
        {
            cmd.Description = "instructs the ninja to hide in a specific location";
            cmd.HelpOption("-?|-h|--help");

            var locationArgument = cmd.Argument("[location]", "where the ninja should hide");

            cmd.OnExecute(() =>
            {
                options.Command = new HideCommand(locationArgument.Value, options.Options);
                return 0;
            });
        }
    }
}