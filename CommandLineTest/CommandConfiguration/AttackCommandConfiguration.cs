﻿using Commands;
using Microsoft.Extensions.CommandLineUtils;

namespace CommandLineTest
{
    public static class AttackCommandConfiguration
    {
        public static void Configure(CommandLineApplication cmd, CommandLineOptions options)
        {
            cmd.Description = "instruct the ninja to attack";
            cmd.HelpOption("-?|-h|--help");

            var excludeOption = cmd.Option("-e|--exclude <exclusions>", "things to exclude from attacking", CommandOptionType.MultipleValue);
            var screamOption  = cmd.Option("-s|--scream", "scream while attacking", CommandOptionType.NoValue);

            cmd.OnExecute(() =>
            {
                options.Command = new AttackCommand(excludeOption.Values, options.Options, screamOption.HasValue());
                return 0;
            });
        }
    }
}