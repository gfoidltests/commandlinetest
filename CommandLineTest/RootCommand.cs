﻿using Commands;
using Microsoft.Extensions.CommandLineUtils;

namespace CommandLineTest
{
    public class RootCommand : ICommand
    {
        private readonly CommandLineApplication _app;
        //---------------------------------------------------------------------
        public RootCommand(CommandLineApplication app) => _app = app;
        //---------------------------------------------------------------------
        public int Run()
        {
            //_app.ShowHelp();
            _app.ShowHint();

            return 1;
        }
    }
}