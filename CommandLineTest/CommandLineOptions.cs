﻿using System;
using Commands;
using Microsoft.Extensions.CommandLineUtils;

namespace CommandLineTest
{
    public class CommandLineOptions
    {
        public ICommand Command      { get; set; }
        public GlobalOptions Options { get; } = new GlobalOptions();
        //---------------------------------------------------------------------
        public static CommandLineOptions Parse(string[] args)
        {
            var cmdOptions = new CommandLineOptions();
            var app = new CommandLineApplication();

            var isQuietOption = app.Option("-q|--extra-quiet", "instruct the ninja to be quiet", CommandOptionType.NoValue);

            RootCommandConfiguration.Configure(app, cmdOptions);

            int result = app.Execute(args);

            cmdOptions.Options.IsQuiet = isQuietOption.HasValue();

            if (result != 0 || cmdOptions.Command == null)
            {
                Console.Error.WriteLine("fail");
                return null;
            }

            return cmdOptions;
        }
    }
}