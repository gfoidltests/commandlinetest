﻿namespace Commands
{
    public interface ICommand
    {
        int Run();
    }
}