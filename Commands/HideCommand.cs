﻿using System;

namespace Commands
{
    public class HideCommand : ICommand
    {
        private readonly string _location;
        private readonly GlobalOptions _options;
        //---------------------------------------------------------------------
        public HideCommand(string location, GlobalOptions options)
        {
            _location = location;
            _options = options;
        }
        //---------------------------------------------------------------------
        public int Run()
        {
            string location = _location ?? "in a trash can";

            Console.WriteLine($"Ninja is hidden in {location}{(_options.IsQuiet ? "pssst" : string.Empty)}");

            return 0;
        }
    }
}