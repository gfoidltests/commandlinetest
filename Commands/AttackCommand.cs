﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Commands
{
    public class AttackCommand : ICommand
    {
        private readonly IEnumerable<string> _exclusions;
        private readonly bool _scream;
        private readonly GlobalOptions _options;
        //---------------------------------------------------------------------
        public AttackCommand(IEnumerable<string> exclusions, GlobalOptions options, bool scream = false)
        {
            _exclusions = exclusions;
            _scream = scream;
            _options = options;
        }
        //---------------------------------------------------------------------
        public int Run()
        {
            var attacking = new string[] { "dragons", "badguys", "civilians", "animals" }.Where(s => !_exclusions.Contains(s));

            Console.Write($"Ninja is attacking {string.Join(", ", attacking)}");

            if (_scream) Console.Write(" while screaming");
            if (_options.IsQuiet) Console.Write(" very quiet");

            Console.WriteLine();

            return 0;
        }
    }
}